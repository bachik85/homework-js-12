// 1).setTimeout() отсрачивает выполнение функции только один раз,  тогда как
// setInterval()  вызывает функцию регулярно, через какой то промежуток времени
// 2). Если задать нулевой setTimeout(),она выполнится после завершения
// выполнения текущего кода.
// 3). Если не использовать clearInterval, то действия в вызванной функии будут
// выполнятся безконечно



const slides = document.querySelectorAll('#imgSlides .img');
let currentImg = 0;
let slideInterval = setInterval(nextImg, 3000);

function nextImg() {
    slides[currentImg].className = 'img';
    currentImg = (currentImg + 1) % slides.length;
    slides[currentImg].className = 'img showing';
}

const pauseButton = document.getElementById('pause');
const continueButton = document.getElementById('continue');

function pauseSlideshow() {
    document.getElementById('pause');
    clearInterval(slideInterval);
}

function playSlideshow() {
    document.getElementById('continue');
    slideInterval = setInterval(nextImg, 3000);

}

pauseButton.onclick = function () {
    pauseSlideshow();
};

continueButton.onclick = function () {
    playSlideshow();
};

